import os
import fuse

STAT_KEYS = ['st_atime', 'st_ctime', 'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid']
STATFS_KEYS = ['f_bavail', 'f_bfree', 'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag', 'f_frsize', 'f_namemax']

class TemplateFileSystem(fuse.Operations):
	def __init__(self):
		pass

	# ---=== Filesystem methods ===---

	# tests for access to file/directory
	# returns nothing
	# raises EACCES error if no access
	def access(self, path, mode):
		...
		#raise fuse.FuseOSError(fuse.EACCES)

	# change mode of file
	# returns nothing
	def chmod(self, path, mode):
		...

	# change ownership of file
	# returns nothing
	def chown(self, path, uid, gid):
		...

	# get file stats (see os.stat)
	# returns a dict of key-value pairs for the stats of the file
	def getattr(self, path fh=None):
		...
		#return {key: value for key in STAT_KEYS}

	# gets the directory listing for path (including . and ..)
	# returns a list of strings
	def readdir(self, path, fh):
		...
		#return ['.', '..'] + [filename for file in path]

	# reads a symbollic link at path
	# Returns a string representing the relative path to which the symbolic link points.
	def readlink(self, path):
		...

	# makes a filesystem node (file, device special file or named pipe) named path
	# returns nothing
	def mknod(self, path, mode, dev):
		...

	# removes the directory named path
	# Only works when the directory is empty, may need to recursivly remove
	# returns nothing
	def rmdir(self, path):
		...

	# makes a new directory named path
	# returns nothing
	def mkdir(self, path, mode):
		...

	# get filesystem stats (see os.statvfs)
	# returns a dict of key-value pairs for the statss of the filesystem
	def statfs(self, path):
		...
		#return {key: value for key in STATFS_KEYS}

	# creates a symbolic link pointing to the target with given name
	# returns nothing
	def symlink(self, target, name):
		...

	# remove (delete) the file named path (indentical to remove)
	# returns nothing
	def unlink(self, path):
		...

	# renames the file/directory named old to new
	# returns nothing
	def rename(self, old, new):
		...

	# creates a hard link pointing to target with given name
	# returns nothing
	def link(self, target, name):
		...

	# set the access and modified times of the file specified by path.
	# times are represented by second since epoch
	# returns nothing
	def utimens(self, path, times=None):
		...

	# ---=== File methods ===---

	# Opens the file at path and set various flags
	# Returns the file descriptor as an int
	def open(self, path, flags):
		...

	# Creates a file at path with given mode
	# Returns the file descriptor as an int
	def create(self, path, mode, fi=None):
		...

	# Reads length bytes starting at offset from file at path using file descriptor fh
	# Returns a sequence of bytes read
	def read(self, path, length, offset, fh):
		...

	# Write bytes in data to file at path starting at offset using file descriptor fh
	# Return the number of bytes written
	def write(self, path, data, offset, fh):
		...

	# Truncates the file at path so it is at most length long
	# Returns nothing
	def truncate(self, path, length, fh=None):
		...

	# Force-writes file to permanent storage
	# returns nothing
	def flush(self, path, fh):
		...

	# Same as flush
	def fsync(self, path, fsyncdata, fh):
		...

	# Close file descriptor fh
	# returns nothing
	def release(self, path, fh):
		...

if __name__ == '__main__':
	import sys

	fuse.FUSE(TemplateFileSystem(), sys.argv[1], foreground=True)