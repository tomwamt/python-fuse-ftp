import fuse
import logging
import os
import time

S_IFREG = 0o100000 # regular file
S_IFDIR = 0o040000 # directory
S_IFLNK = 0o120000 # symbolic link

STAT_KEYS = ['st_atime', 'st_ctime', 'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid']
STATFS_KEYS = ['f_bavail', 'f_bfree', 'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag', 'f_frsize', 'f_namemax']

class FSObject():
	def __init__(self, perms):
		self.perms = perms
		now = time.time()
		self.change_time = now # status change
		self.access_time = now
		self.modify_time = now
		self.owner = os.getuid()
		self.group = os.getgid()
		self.links = 1
		self.xattr = {}

	def stat(self):
		return {
			'st_atime': self.access_time, 
			'st_ctime': self.change_time, 
			'st_gid': self.group, 
			'st_mode': self.type | self.perms, 
			'st_mtime': self.modify_time, 
			'st_nlink': self.links, 
			'st_size': self.size(), 
			'st_uid': self.owner
		}

class File(FSObject):
	def __init__(self, perms):
		self.type = S_IFREG
		super(File, self).__init__(perms)
		self.data = bytearray()

	def size(self):
		return len(self.data)
		
class Directory(FSObject):
	def __init__(self, perms):
		self.type = S_IFDIR
		super(Directory, self).__init__(perms)
		self.contents = {}

	def size(self):
		return 0

class MemoryFS(fuse.LoggingMixIn, fuse.Operations):
	def __init__(self):
		self.root = Directory(0o755)

	def get_file(self, path):
		patht = tuple(filter(None, path.split('/')))
		f = self.root
		for d in patht:
			if isinstance(f, Directory) and d in f.contents:
				f = f.contents[d]
			else:
				#print('Invalid path: '+path)
				raise fuse.FuseOSError(fuse.ENOENT)
		return f

	def exists(self, path):
		path = tuple(filter(None, path.split('/')))
		f = self.root
		for d in path:
			if isinstance(f, Directory) and d in f.contents:
				f = f.contents[d]
			else:
				return False
		return True

	# ---=== Filesystem methods ===---

	# tests for access to file/directory
	# returns nothing
	# raises EACCES error if no access
	#def access(self, path, mode):
		#...
		#raise fuse.FuseOSError(fuse.EACCES)

	# change mode of file
	# returns nothing
	def chmod(self, path, mode):
		f = self.get_file(path)
		f.perms = 0o000777 & mode

	# change ownership of file
	# returns nothing
	def chown(self, path, uid, gid):
		f = self.get_file(path)
		f.owner = uid
		f.group = gid

	# get file stats (see os.stat)
	# returns a dict of key-value pairs for the stats of the file
	def getattr(self, path, fh=None):
		return self.get_file(path).stat()

	def getxattr(self, path, name, position=0):
		try:
			return self.get_file(path).xattr[name]
		except KeyError:
			return b''

	def setxattr(self, path, name, value, options, position=0):
		self.get_file(path).xattr[name] = bytes(value)

	def listxattr(self, path):
		return list(self.get_file(path).xattr.keys())

	def removexattr(self, path, name):
		try:
			del self.get_file(path).xattr[name]
		except KeyError:
			pass

	# gets the directory listing for path (including . and ..)
	# returns a list of strings
	def readdir(self, path, fh):
		f = self.get_file(path)
		if isinstance(f, Directory):
			return ['.', '..'] + list(f.contents.keys())

	# removes the directory named path
	# Only works when the directory is empty, may need to recursivly remove
	# returns nothing
	def rmdir(self, path):
		if path == '/':
			return

		parentpath, name = path.rstrip('/').rsplit('/', 1)
		f = self.get_file(path)
		parent = self.get_file(parentpath)
		if isinstance(f, Directory) and len(f.contents) == 0:
			del parent.contents[name]

	# makes a new directory named path
	# returns nothing
	def mkdir(self, path, mode):
		if path == '/':
			return

		parentpath, name = path.rstrip('/').rsplit('/', 1)
		parent = self.get_file(parentpath)
		if isinstance(parent, Directory) and name not in parent.contents:
			perms = 0o000777 & mode
			parent.contents[name] = Directory(perms)

	# get filesystem stats (see os.statvfs)
	# returns a dict of key-value pairs for the statss of the filesystem
	def statfs(self, path):
		return {
			'f_namemax': 255,
			'f_bsize': 512,
			'f_blocks': 3906250, #2GB
			'f_bfree': 3906250,
			'f_bavail': 3906250,
			'f_files': 4096,
			'f_ffree': 4096,
			'f_favail': 4096
		}

	# renames the file/directory named old to new
	# returns nothing
	def rename(self, old, new):
		if old == '/' or new == '/':
			return

		oldParentPath, oldName = old.rstrip('/').rsplit('/', 1)
		newParentPath, newName = new.rstrip('/').rsplit('/', 1)

		oldParent = self.get_file(oldParentPath)
		newParent = self.get_file(newParentPath)

		file = oldParent.contents[oldName]
		del oldParent.contents[oldName]
		newParent.contents[newName] = file

	# set the access and modified times of the file specified by path.
	# times are represented by second since epoch
	# returns nothing
	def utimens(self, path, times=None):
		if times is None:
			now = time.time()
			atime = now
			mtime = now
		else:
			atime, mtime = times

		f = self.get_file(path)
		f.access_time = atime
		f.modify_time = mtime

	# ---=== File methods ===---

	# Opens the file at path and set various flags
	# Returns the file descriptor as an int
	def open(self, path, flags):
		f = self.get_file(path)
		if not isinstance(f, Directory):
			f.access_time = time.time()
			return hash(path.rstrip('/')) % 0x7fffffff

	# Close file descriptor fh
	# returns nothing
	def release(self, path, fh):
		pass

	# Creates a file at path with given mode
	# Returns the file descriptor as an int
	def create(self, path, mode, fi=None):
		parentpath, name = path.rstrip('/').rsplit('/', 1)
		parent = self.get_file(parentpath)

		if isinstance(parent, Directory) and name not in parent.contents:
			perms = 0o000777 & mode
			parent.contents[name] = File(perms)

		return hash(path.rstrip('/')) % 0x7fffffff

	# remove (delete) the file named path
	# returns nothing
	def unlink(self, path):
		if path == '/':
			return

		parentpath, name = path.rstrip('/').rsplit('/', 1)
		parent = self.get_file(parentpath)

		del parent.contents[name]

	# Reads length bytes starting at offset from file at path using file descriptor fh
	# Returns a sequence of bytes read
	def read(self, path, length, offset, fh):
		f = self.get_file(path)
		if isinstance(f, File):
			return bytes(f.data[offset:offset+length])

	# Write bytes in data to file at path starting at offset using file descriptor fh
	# Return the number of bytes written
	def write(self, path, data, offset, fh):
		f = self.get_file(path)
		length = len(data)
		if isinstance(f, File):
			if f.size() < offset+length:
				toextend = offset+length - f.size()
				f.data.extend(bytes(toextend))
			f.data[offset:offset+length] = bytes(data)
		f.modify_time = time.time()
		return len(data)

	# Truncates the file at path so it is at most length long
	# Returns nothing
	def truncate(self, path, length, fh=None):
		f = self.get_file(path)
		if isinstance(f, File):
			f.data = f.data[:length]

if __name__ == '__main__':
	import sys
	# usage: memory.py <mountpoint>

	#print('call: {} path: {}'.format('chmod', path))
	logging.basicConfig(level=logging.DEBUG)
	fuse.FUSE(MemoryFS(), sys.argv[1], foreground=True)