from datetime import datetime, timezone, timedelta
from io import BytesIO
import os
import fuse
from ftplib import FTP

STAT_KEYS = ['st_atime', 'st_ctime', 'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid']
STATFS_KEYS = ['f_bavail', 'f_bfree', 'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag', 'f_frsize', 'f_namemax']

# turns an FTP file modify time into a UNIX timestamp
def parse_time(timestr):
	y = int(timestr[0:4])
	mo = int(timestr[4:6])
	d = int(timestr[6:8])
	h = int(timestr[8:10])
	mi = int(timestr[10:12])
	s = int(timestr[12:14])
	return datetime(y, mo, d, h, mi, s, tzinfo=timezone(-timedelta(hours=0))).timestamp()

# Implements filesystem operations
class FTPFileSystem(fuse.LoggingMixIn, fuse.Operations):
	# establishes a connection and intiializes caches
	def __init__(self, address, user, passw):
		self.address = address
		self.user = user
		self.passw = passw
		self.ftp = FTP(address)
		self.ftp.login(user, passw)

		self.openedFiles = {} # cached file data
		self.stats = {} # cached file stats

	# closes out the connection
	def destroy(self, path):
		self.ftp.quit()
		self.ftp.close()

	# verifies the connection. If a simple command fails, reestablish it
	def verify_connect(self):
		try:
			self.ftp.pwd()
		except:
			self.ftp = FTP(self.address)
			self.ftp.login(self.user, self.passw)

	# given a path, calls get_all_stats() on the parent directory
	def refresh_parent(self, path):
		parent = path.rsplit('/')[0]
		if parent == '':
			parent = '/'
		self.get_all_stats(parent)

	# gets information about all files in path
	# path should be a directory
	def get_all_stats(self, path):
		self.verify_connect()

		if path == '/': 
			path = '' # make root empty string directory
		else:
			print(self.ftp.cwd(path)) # if not root, change to directory
		fs = list(self.ftp.mlsd()) # Use FTP MLSD command to get files and details
		self.ftp.cwd('/') # change back to root

		# iterate through what was returned by MLSD
		for filename, details in fs:
			if filename == '.' or filename == '..':
				continue

			time = parse_time(details['modify'])
			if details['type'] == 'dir': # directory
				mode = 0o040000 | 0o755 # file mode
				size = int(details['sizd']) # ??? size of a directory?
			else: # file
				mode = 0o100000 | 0o644
				size = int(details['size'])

			# cache the stat structure
			self.stats[path+'/'+filename] = {
				'st_atime': time, 
				'st_ctime': time, 
				'st_gid': os.getgid(), 
				'st_mode': mode, 
				'st_mtime': time, 
				'st_nlink': 2, 
				'st_size': size, 
				'st_uid': os.getuid()
			}

	# ---=== Filesystem methods ===---

	# get file stats (see os.stat)
	# returns a dict of key-value pairs for the stats of the file
	def getattr(self, path, fh=None):
		if path == '/': # special case for root
			return {
				'st_atime': 0, 
				'st_ctime': 0, 
				'st_gid': os.getgid(), 
				'st_mode': 0o040000 | 0o666, 
				'st_mtime': 0, 
				'st_nlink': 2, 
				'st_size': 4096, 
				'st_uid': os.getuid()
			}
		else:
			if path in self.stats: # we know about this file
				return self.stats[path]
			else:
				raise fuse.FuseOSError(fuse.ENOENT) # File not found error

	# gets stats for all files in this directory
	def opendir(self, path):
		self.get_all_stats(path)
		return 0

	# gets the directory listing for path (including . and ..)
	# returns a list of strings
	def readdir(self, path, fh):
		result = []
		for key in self.stats.keys(): # for everything in the cached file info
			if self.stats[key] is None:
				continue
			parent, name = key.rsplit('/', maxsplit=1) # split the path into parent directory and filename
			if parent == '':
				parent = '/' # empty string parent means root
			if parent == path: # if parent is what we're looking at
				result.append(name)

		return ['.', '..'] + result

	# removes the directory named path
	# Only works when the directory is empty, may need to recursivly remove
	# returns nothing
	def rmdir(self, path):
		self.verify_connect()
		self.ftp.rmd(path) # FTP RMD command

	# makes a new directory named path
	# returns nothing
	def mkdir(self, path, mode):
		self.verify_connect()
		self.ftp.mkd(path) # FTP MKD command

	# get filesystem stats (see os.statvfs)
	# returns a dict of key-value pairs for the statss of the filesystem
	def statfs(self, path):
		return { # arbitrary info, it doesn't really matter
			'f_namemax': 255,
			'f_bsize': 512,
			'f_blocks': 3906250, #2GB
			'f_bfree': 3906250,
			'f_bavail': 3906250,
			'f_files': 4096,
			'f_ffree': 4096,
			'f_favail': 4096
		}

	# remove (delete) the file named path (indentical to remove)
	# returns nothing
	def unlink(self, path):
		self.verify_connect()
		self.ftp.delete(path) # FTP DELE command
		del self.stats[path]
		self.refresh_parent(path) # update cache

	# renames the file/directory named old to new
	# returns nothing
	def rename(self, old, new):
		self.verify_connect()
		self.ftp.rename(old, new) # FTP RNFR, RNTO commands

		# update cache
		oldStats = self.stats[old]
		del self.stats[old]
		self.stats[new] = oldStats

	# ---=== File methods ===---

	# Opens the file at path and set various flags
	# Returns the file descriptor as an int
	def open(self, path, flags):
		self.verify_connect()
		self.openedFiles[path] = bytearray() # intialize a bytearray in the cache

		# callback to be called per transfered block
		def open_block(block):
			self.openedFiles[path] += bytearray(block)

		# FTP RETR command
		self.ftp.retrbinary('RETR '+path, open_block)
		
		return hash(path)

	# Creates a file at path with given mode
	# Returns the file descriptor as an int
	def create(self, path, mode, fi=None):
		self.verify_connect()
		# FTP STOR command to store an empty file (creates it on the remote server)
		self.ftp.storbinary('STOR '+path, BytesIO(bytes()))
		# creates an empty bytearray in the cache
		self.openedFiles[path] = bytearray()
		self.refresh_parent(path)
		return hash(path)

	# Reads length bytes starting at offset from file at path using file descriptor fh
	# Returns a sequence of bytes read
	def read(self, path, length, offset, fh):
		# reads bytes from cache
		return bytes(self.openedFiles[path][offset:offset+length])

	# Write bytes in data to file at path starting at offset using file descriptor fh
	# Return the number of bytes written
	def write(self, path, data, offset, fh):
		length = len(data)
		# write bytes to cache
		self.openedFiles[path][offset:offset+length] = bytearray(data)
		self.flush(path, fh) # send the data
		return length

	# Truncates the file at path so it is at most length long
	# Returns nothing
	def truncate(self, path, length, fh=None):
		self.openedFiles[path] = self.openedFiles[path][:length]

	# Force-writes file to permanent storage
	# returns nothing
	def flush(self, path, fh):
		self.verify_connect()
		# FTP STOR command to write the bytes back to the sever
		self.ftp.storbinary('STOR '+path, BytesIO(bytes(self.openedFiles[path])))
		self.refresh_parent(path)

	# Same as flush
	def fsync(self, path, fsyncdata, fh):
		self.flush(path, fh)

	# Close file descriptor fh
	# returns nothing
	def release(self, path, fh):
		# clears the cache for this file
		del self.openedFiles[path]

if __name__ == '__main__':
	import sys
	import logging

	logging.basicConfig(level=logging.DEBUG)
	# Mount the filesystem and start serving requests
	fuse.FUSE(FTPFileSystem(*sys.argv[1:4]), 'mnt', foreground=True)